/**
 * 
 * Allegrova 
 * 
 * Читает категории молотка из AMF-файла в котором хранит свой кэш приложение FlexLoader
 * Файлы Cats.dat и SellFormType.dat должны быть расположены в директории data, запускаемого приложения 
 * 
 * Как пользоваться:
 * 
 * 1. открываем FlexBuilder
 * 2. запускаем приложение в дебаггере
 * 3. в консоли видим информацию о каталоге Молотка
 * 
 */
package
{
	import flash.display.Sprite;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	
	public class Allegrova extends Sprite
	{
		
		// Название файла с базой категорий
		private var _catsFile:String = "Cats.dat";
		
		// Название файла с базой параметров
		private var _formTypesFile:String = "SellFormType.dat";
		
		public function Allegrova()
		{
			this._traceAllegroDataFromFile("data/" + this._catsFile);
			this._traceAllegroDataFromFile("data/" + this._formTypesFile);					
		}
		
		/**
		 * Выводит в консоль данные AMF-файла в виде сериализованного json'а
		 */
		public function _traceAllegroDataFromFile(param1:String) : void
		{
			
			var filestream:FileStream = null;			
			var file:File = File.applicationDirectory.resolvePath(param1);
			var object:Object = null;
			var records:Array = new Array();
			var json:String = null;			
			
			if(file.exists)
			{
				filestream = new FileStream();
				filestream.open(file,FileMode.READ);
				
				filestream.position = 0;
				
				while(filestream.bytesAvailable > 0)
				{
					records.push(filestream.readObject());					
				}
				
				object = null;
				filestream.close();
				
				trace(JSON.stringify(records));
			}
		}				
	}
}